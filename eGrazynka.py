#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Rafał Sarniak  

import sys
import time
import threading

from PyQt5.QtWidgets import QDialog, QApplication, QWidget, QMainWindow, QFileDialog
from UI_Licznik import *
from Licznik_SQL import LicznikSQLite

class Licznik(QMainWindow, Ui_MainWindow):
    def __init__(self):
        super(Licznik, self).__init__()
        self.setupUi(self)
        self.Typ_operacji = None
        self.System       = None
        self.dateNtime    = self.dateTimeEdit.dateTime().toPyDateTime().isoformat(sep='T')
        self.comment      = None
        self.db           = None
        self.Button_dodaj_wpis.clicked.connect( self.BTN_dodaj_wpis )
        self.textEdit.textChanged.connect(self.TXT_edit)
        self.dateTimeEdit.dateTimeChanged.connect(self.DTE_changed)
        self.buttonGroup.buttonClicked.connect(self.RBTN_systems)
        self.buttonGroup_2.buttonClicked.connect(self.RBTN_operation)
        self.actionNew.triggered.connect(self.New) 
        self.actionOpen.triggered.connect(self.Open) 
        
    def Open(self):
        filename = QFileDialog.getOpenFileName(caption="Open Database",                                                        directory=".", filter="*.db")[0]
        self.db = LicznikSQLite( filename )
    
    def New(self):
        filename = QFileDialog.getSaveFileName(caption="New Database", directory=".", filter="*.db")[0]
        self.db = LicznikSQLite( filename )
        
    def RBTN_operation(self, button):
        self.Typ_operacji = button.text()
        if self.Typ_operacji in ["Serwis", "Awaria", "Inne", "Niewykorzystane"]:
            self.System = None
    
    def RBTN_systems(self, button):
        self.System = button.text()
        
    def DTE_changed(self):
        self.dateNtime = self.dateTimeEdit.dateTime().toPyDateTime().isoformat(sep='T')
        
    def TXT_edit(self):
        self.comment = self.textEdit.toPlainText()
    
    def BTN_dodaj_wpis(self):
        if self.db == None:
            self.stworz_baze_sam()
        print(self.dateNtime, self.Typ_operacji, self.System, self.comment)
        self.db.add_wpis_ogolne_operacje(self.dateNtime, self.Typ_operacji, self.System, self.comment)
        self.textEdit.clear()
        
    def stworz_baze_sam(self):
        filename = "default.db"
        print( "Tworzę/otwieram domyślną bazę o nazwie %s" % filename )
        self.db = LicznikSQLite( filename )



if __name__=="__main__":    
    app    = QApplication(sys.argv)
    w = Licznik()
    w.show()
    sys.exit(app.exec_())
