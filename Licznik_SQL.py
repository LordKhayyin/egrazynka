#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from __future__ import print_function
""" @package Licznik_SQL.py
  \brief     Obsługa baz danych dla Licznik.
  
  \author    Rafał Sarniak
"""

import sqlite3
from sqlite3 import Error
import pandas as pd
from collections import OrderedDict

class LicznikSQLite:
    def __init__(self, path):
        self.path_to_db = path
        self.conn       = self.create_connection(self.path_to_db)
        self.create_table_ogolne_operacje()
        
    def create_connection(self, db_file):
        """ create a database connection to the SQLite database
            specified by db_file
        :param db_file: database file
        :return: Connection object or None
        """
        conn = None
        try:
            conn = sqlite3.connect(db_file)
            return conn
        except Error as e:
            print(e)
        return conn
    
    def wykonaj(self, sql):
        try:
            cur=self.conn.cursor()
            cur.execute(sql)
            self.conn.commit()
        except Error as e:
            print(e)
            
    def check_is_already_there(self, table, column, column_value):
        sql = "SELECT EXISTS(SELECT 1 FROM %s WHERE %s='%s' LIMIT 1)" % ( table, column, column_value) 
        cur = self.conn.cursor()
        cur.execute(sql)
        if cur.fetchone()[0] == 1:
            return True
        else:
            return False
        
    def create_table_ogolne_operacje(self):
        sql_create_ogolne_operacje = """CREATE TABLE IF NOT EXISTS ogolne_operacje (
                                    id integer PRIMARY KEY,
                                    timestamp text,
                                    operation text,
                                    receiver  text,
                                    comment   text,
                                    unique(timestamp, operation, receiver)
                                );"""
        self.wykonaj(sql_create_ogolne_operacje)
    
    def add_new_column(self, table_name, column_name, column_type = "text" ):
        sql = "ALTER TABLE %s ADD COLUMN %s %s" % (table_name, column_name, column_type)
        self.wykonaj(sql)
        
        
    def get_nice_output(self, qry, index_col=None):
        return pd.io.sql.read_sql(qry, self.conn, index_col=index_col)
    
    def add_wpis_ogolne_operacje(self, timestamp, operation, receiver, comment):
        sql = ''' INSERT INTO ogolne_operacje(timestamp, operation, receiver, comment)
                  VALUES(?,?,?,?) '''
        cur = self.conn.cursor()
        try:
            cur.execute(sql, (timestamp, operation, receiver, comment) )
            self.conn.commit()
        except Error as e:
            print(e)
        return cur.lastrowid

        
if __name__ == "__main__":
    sq = LicznikSQLite("BazaTestowa.db")
    #sq.create_table_ogolne_operacje()
    sq.add_wpis_ogolne_operacje("2023-12-21T18:00:00", "VLBI", "X", "Takie tam, z obserwacji.")
    sq.add_wpis_ogolne_operacje("2023-12-22T04:00:00", "eVLBI","L", "Bo tak.")
